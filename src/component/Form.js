import React from 'react';

let styleForm = {
    width:'50%',
    height:'5em'
}

const mediaQuery=window.matchMedia('(min-width: 480px)')
if(!(mediaQuery.matches)) {
    styleForm = {
        width:'200px',
        height:'5em'
    }
}

class Form extends React.Component  {
    constructor(){
        super()
        this.state = {
          city: '',
          country: ''
        }
      }

    render() {
        return (
            
            <form style = {styleForm} onSubmit={this.props.loadData}  >
                <input onChange={this.props.onChangeCity}  style={{height:"2em", backgroundColor:'#1d3557', color:"white"}} type='text'  placeholder='City' name='city'></input>
                <input onChange={this.props.onChangeCountry}  style={{height:"2em", backgroundColor:'#1d3557',color:"white"}} type='text'  placeholder='Country' name='country'></input>
                <button style={{height:"2.4em", backgroundColor:'rgb(230 189 52)'}} type='submit' > Get Weather </button>
                {this.props.error_city ? <span style={{height:"2em", backgroundColor:'#1d3557', color:"red"}}> Please enter city name</span> :""}
                {this.props.error_country ? <span style={{height:"2em", backgroundColor:'#1d3557', color:"red"}}> Please enter Country name</span> :""}
            </form>
        );
    }
}

export default Form;