import React from 'react';
const cardStyle = {
    height: '300px',
    width: '150px',
    backgroundColor:'#D4F1F4',
    borderRadius:'10px'
}

const HourlyWeather = (props) => {
        return (
            <div style={cardStyle}>
                <h3>{props.city}</h3>
                <img src={props.icon} style={{width:'80px', height:"80px"}} alt="weatherLogo"/>
                <h2>{props.temp}&deg;C</h2>
                {minAndMaxTemp(props.minimumTemperature,props.maximumTemperature)}
                <h3>{props.weatherDescription}</h3>
            </div>
        );
}

function minAndMaxTemp(min,max) {
    return (
    <h5 style={mix_max_style}>
    <span>{min}&deg;C</span>
    <span>{max}&deg;C</span>
    </h5>
    )
}

const mix_max_style = {
    display: 'flex',
    justifyContent:'space-around',
}


export default HourlyWeather;