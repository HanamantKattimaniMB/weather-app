import React, { Component } from 'react';
import weatherLogo from '../images/01d.png';
import facebookLogo from '../images/SocialMediaLogo/icon-facebook.svg'
import instagramLogo from '../images/SocialMediaLogo/icon-instagram.svg'
import twitterLogo from '../images/SocialMediaLogo/icon-twitter.svg'
import youtubeLogo from '../images/SocialMediaLogo/icon-youtube.svg'
import pinterestLogo from '../images/SocialMediaLogo/icon-pinterest.svg'



class Footer extends Component {
    render() {
        return (
            <div style={footerStyle}>
                <div style={{flexDirection:'column', justifyContent:'space-around'}}>
                    <img src={weatherLogo} style={mainLogoStyle} alt="mainLogo"/>
                    <div>
                        <img src={facebookLogo} style={socialMediaLogoStyle} alt='facebook'/>
                        <img src={instagramLogo} style={socialMediaLogoStyle} alt='instagram'/>
                        <img src={twitterLogo} style={socialMediaLogoStyle} alt='twitter'/>
                        <img src={youtubeLogo} style={socialMediaLogoStyle} alt="youtube"/>  
                        <img src={pinterestLogo} style={socialMediaLogoStyle} alt="pinterest"/>    
                    </div>
                </div>
                <div>
                <h3>Contact</h3>
                <h6>Phone: 2142124121</h6>
                <h6>Email: dummy@gmail.com</h6>
                </div>
            </div>
        );
    }
}

const footerStyle = {
    flexDirection:"row",
    backgroundColor:'#1d3557',
    width:'100%', 
    height:'14em', 
    marginTop:"10em", 
    color:'white',
}

const mainLogoStyle={
    height: "50px",
    width: "50px",
  }

const socialMediaLogoStyle= {
    height: "20px",
    width: "20px",
}
export default Footer;