import React, { Component } from 'react'
import weatherLogo from '../images/01d.png';

class Header extends Component {
    render() {
        return (
            <header>
                <img src={weatherLogo} alt='mainLogo'/>
                <p>Next Five days Weather Forecast</p>
            </header>
        )
    }
}
export default Header