import React from 'react';
import '../App.css'
let cardStyle = {
    flexDirection:'row',
    height: '300px',
    width: '190px',
    backgroundColor:'#0c70bc',
    borderRadius:'10px',
    opacity: "0.8",
}

let imgStyle = {
    width:'80px', height:"80px"
}
const mediaQuery=window.matchMedia('(min-width: 480px)')
if(!(mediaQuery.matches)) {
    cardStyle = {
        height: '250px',
        width: '300px',
        backgroundColor:'#0c70bc',
        borderRadius:'10px',
        flexWrap:'wrap',
        opacity: "0.8",
        marginTop:"7px"      
    }

    imgStyle = {
        width:'50px', height:"50px"
    }
    console.log('matched')
}

const WeatherCard = (props) => {
        return (

            <div style={cardStyle}>
                <h3>{props.city}</h3>
                <img src={props.icon} style={imgStyle} alt="weatherLogo"/>
                <h2>{props.temp}&deg;C</h2>
                {minAndMaxTemp(props.minimumTemperature,props.maximumTemperature)}
                <h3>{props.weatherDescription}</h3>
            </div>
        );
}

function minAndMaxTemp(min,max) {
    return (
    <h5 style={mix_max_style}>
    <span>Min:{min}&deg;C</span>
    <span>Max:{max}&deg;C</span>
    </h5>
    )
}

const mix_max_style = {
    display: 'flex',
    justifyContent:'space-around',
}



export default WeatherCard;

  
  