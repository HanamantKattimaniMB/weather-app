import React from 'react';
import './App.css';

import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import sunny from './images/01d.png'
import rainy from './images/09n.png'
import cloudy from './images/03d.png'
import snowy from './images/13d.png'

import Header from './component/Header'
import WeatherCard from './component/WeatherCard';
import Footer from './component/Footer'
import HourlyWeather from './component/HourlyWeather'
import Form from './component/Form';
import "./App.css"

const API_KEY="c450ce9bee5985c1ba34a3b243a38727"

class App extends React.Component {
  constructor() {
    super()
    this.state = { matches: window.matchMedia("(min-width: 768px)").matches };
    this.state = {
      city:'pune',
      country: 'india',
      temp:undefined,
      Clouds:undefined,
      minimumTemperature:undefined,
      weatherDescription:undefined,
      THUNDER_LOGO:undefined,
      icon:undefined,
      error_city:false,
      error_country:false,
    }
  }

  componentDidMount() {
    this.getWeather()
  }

onChangeCity = (e) => {
  console.log('city: ', this.state.city)
  this.setState({city:e.target.value})
}

onChangeCountry = (e) => {
  this.setState({country:e.target.value})
}

  getWeather = async (event) =>{
    if(event) {
      event.preventDefault()
    }

    console.log(event)
    const city=event === undefined ?"pune":event.target.elements.city.value 
    const country=event ===undefined ?"india":event.target.elements.country.value 

    if(city==='') {
      this.setState({error_city:true})
    } else {
      this.setState({error_city:false})
    }

    if(country==='') {
      this.setState({error_country:true})
    } else {
      this.setState({error_country:false})
    }
  
     let call_api = await fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${this.state.city},${this.state.country}&appid=${API_KEY}`)    
    
    const response = await call_api.json()
    console.log(response)

    function kelvinToCelsius(temperatureInKelvin) {
      return (temperatureInKelvin-273.15).toFixed(2)
    }
    
    let startingTime=response.list[0].dt
    const firstDay=response.list.filter(x=> x.dt===(startingTime+4*10800))
    const secondDay=response.list.filter(x=> x.dt===(startingTime+8*10800))
    const thirdDay=response.list.filter(x=> x.dt===(startingTime+16*10800))
    const fourthDay=response.list.filter(x=> x.dt===(startingTime+32*10800))
    const fifthDay=response.list.filter(x=> x.dt===(startingTime+35*10800))

    this.setState({
      city:response.city.name,
      temp:kelvinToCelsius(firstDay[0].main.temp),
      minimumTemperature:kelvinToCelsius(firstDay[0].main.temp_min),
      maximumTemperature:kelvinToCelsius(firstDay[0].main.temp_max),
      weatherDescription:firstDay[0].weather[0].description,

      temp_2d:kelvinToCelsius(secondDay[0].main.temp),
      minimumTemperature_2d:kelvinToCelsius(secondDay[0].main.temp_min),
      maximumTemperature_2d:kelvinToCelsius(secondDay[0].main.temp_max),
      weatherDescription_2d:secondDay[0].weather[0].description,

      temp_3d:kelvinToCelsius(thirdDay[0].main.temp),
      minimumTemperature_3d:kelvinToCelsius(thirdDay[0].main.temp_min),
      maximumTemperature_3d:kelvinToCelsius(thirdDay[0].main.temp_max),
      weatherDescription_3d:thirdDay[0].weather[0].description,

      temp_4d:kelvinToCelsius(fourthDay[0].main.temp),
      minimumTemperature_4d:kelvinToCelsius(fourthDay[0].main.temp_min),
      maximumTemperature_4d:kelvinToCelsius(fourthDay[0].main.temp_max),
      weatherDescription_4d:fourthDay[0].weather[0].description,

      temp_5d:kelvinToCelsius(fifthDay[0].main.temp),
      minimumTemperature_5d:kelvinToCelsius(fifthDay[0].main.temp_min),
      maximumTemperature_5d:kelvinToCelsius(fifthDay[0].main.temp_max),
      weatherDescription_5d:fifthDay[0].weather[0].description,
    })
    let range=firstDay[0].weather[0].id
    let range_1d=secondDay[0].weather[0].id
    let range_2d=thirdDay[0].weather[0].id
    let range_3d=fourthDay[0].weather[0].id
    let range_4d=fifthDay[0].weather[0].id

    console.log(range,range_1d,range_2d,range_3d,range_4d)
   
    switch(true) {
      case range>=200 && range<=232 : 
        this.setState({icon:sunny})
        break
      case range>=500 && range<=531 : 
        this.setState({icon:rainy})
        break
      case range===800 : 
        this.setState({icon:snowy})
        break
      case range>=801 && range<=804 : 
        this.setState({icon:cloudy})
        break
      default:
        this.setState({icon:cloudy})
    }

    switch(true) {
        case range_1d>=200 && range_1d<=232 : 
          this.setState({icon_1d:sunny})
          break
        case range_1d>=500 && range_1d<=531 : 
          this.setState({icon_1d:rainy})
          break
        case range_1d===800 : 
          this.setState({icon_1d:snowy})
          break
        case range_1d>=801 && range_1d<=804 : 
          this.setState({icon_1d:cloudy})
          break
        default:
          this.setState({icon:cloudy})
      }

      switch(true) {
        case range_2d>=200 && range_2d<=232 : 
          this.setState({icon_2d:sunny})
          break
        case range_2d>=500 && range_2d<=531 : 
          this.setState({icon_2d:rainy})
          break
        case range_2d===800 : 
          this.setState({icon_2d:snowy})
          break
        case range_2d>=801 && range_2d<=804 : 
          this.setState({icon_2d:cloudy})
          break
        default:
          this.setState({icon:cloudy})
      }

      switch(true) {
        case range_3d>=200 && range_3d<=232 : 
          this.setState({icon_3d:sunny})
          break
        case range_3d>=500 && range_3d<=531 : 
          this.setState({icon_3d:rainy})
          break
        case range_3d===800 : 
          this.setState({icon_3d:snowy})
          break
        case range_3d>=801 && range_3d<=804 : 
          this.setState({icon_3d:cloudy})
          break
        default:
          this.setState({icon:cloudy})
      }

      switch(true) {
        case range_4d>=200 && range_4d<=232 : 
          this.setState({icon_4d:sunny})
          break
        case range_4d>=500 && range_4d<=531 : 
          this.setState({icon_4d:rainy})
          break
        case range_4d===800 : 
          this.setState({icon_4d:snowy})
          break
        case range_4d>=801 && range_4d<=804 : 
          this.setState({icon_4d:cloudy})
          break
        default:
          this.setState({icon:cloudy})
      }
  }

  render() {
    return (
      <div className="App">
      <Router>
        <Header className='myHeader'/>
        <Route exact path='/'>

         <Form loadData={this.getWeather} 
              onChangeCity={this.onChangeCity} 
              error_city={this.state.error_city}  
              error_country={this.state.error_country}  
              onChangeCountry={this.onChangeCountry}/>

         </Route>
        <div className='weather_container'>
          <WeatherCard className='card' city = {this.state.city} 
                      temp = {this.state.temp}
                      minimumTemperature = {this.state.minimumTemperature}
                      maximumTemperature = {this.state.maximumTemperature}
                      weatherDescription = {this.state.weatherDescription}
                      icon = {this.state.icon}/>

          <WeatherCard city = {this.state.city} 
                      temp = {this.state.temp_2d}
                      minimumTemperature = {this.state.minimumTemperature_2d}
                      maximumTemperature = {this.state.maximumTemperature_2d}
                      weatherDescription = {this.state.weatherDescription_2d}
                      icon = {this.state.icon_1d}/>

          <WeatherCard city = {this.state.city} 
                      temp = {this.state.temp_3d}
                      minimumTemperature = {this.state.minimumTemperature_3d}
                      maximumTemperature = {this.state.maximumTemperature_3d}
                      weatherDescription = {this.state.weatherDescription_3d}
                      icon = {this.state.icon_2d}/>

          <WeatherCard city = {this.state.city} 
                      temp = {this.state.temp_5d}
                      minimumTemperature = {this.state.minimumTemperature_4d}
                      maximumTemperature = {this.state.maximumTemperature_4d}
                      weatherDescription = {this.state.weatherDescription_4d}
                      icon = {this.state.icon_3d}/>

          <WeatherCard city = {this.state.city} 
                      temp = {this.state.temp_5d}
                      minimumTemperature = {this.state.minimumTemperature_5d}
                      maximumTemperature = {this.state.maximumTemperature_5d}
                      weatherDescription = {this.state.weatherDescription_5d}
                      icon = {this.state.icon_4d}/>
        </div>

        
        <Route exact path='/day'>
            <HourlyWeather city = {this.state.city} 
            temp = {this.state.temp_3d}
            minimumTemperature = {this.state.minimumTemperature_3d}
            maximumTemperature = {this.state.maximumTemperature_3d}
            weatherDescription = {this.state.weatherDescription_3d}
            icon = {this.state.icon}/> 
        </Route>
        </Router>
        <Footer/>
      </div>
    );
  }
}

export default App;












